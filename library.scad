///////////////////////
// shared parameters //
///////////////////////

wall = 2; // thickness of the thinnest wall
base = 3; // thickness of the box base

tolerance = 0.5; // bigger gap allows smoother stacking

reinforcement = 3; // height of vertical wall reinforcement

notch_width = 7; // x-y size of the bottom notch
notch_height = 10; // z size of the bottom notch

hole_width = 40; // size of the hole for holding
hole_height = 20; // size of the hole for holding

radius_inner = 4; // radius of the inner storage area
radius_outer = 1; // radius of outer corners and top edges
radius_fillet = 1.5; // radius of filleted edges

///////////////////
// general tools //
///////////////////

module fillet(r)
  offset(r = -r, $fn=4)
    offset(delta = +r, $fn=4)
      children();

module chamfer(r)
  offset(r = +r, $fn=4)
    offset(delta = -r, $fn=4)
      children();

// cube rounded on all edges
module round_cube(dims, radius, fn = 8)
  minkowski() {
      cube(dims - radius * [2,2,2], center = true);
        sphere(r = radius, $fn = fn);
  }

// cube chamfered on horizontal edges
module chamfer_cube(dims, radius = 2,
                    x_dimension = true,
                    vertical = false)
  intersection()
  {
    rotate([90, 0, 0])
      linear_extrude(dims.y, center = true)
        chamfer(radius)
          square([dims.x, dims.z], center = true);

    if (!vertical)
      rotate([0, 90, 0])
        linear_extrude(dims.x, center = true)
          chamfer(radius)
            square([dims.z, dims.y], center = true);

    if (vertical)
        linear_extrude(dims.z, center = true)
          chamfer(radius)
            square([dims.x, dims.y], center = true);
  }

/////////////
// the box //
/////////////

function protrusion() = notch_width - wall;

// side view of the box
module side_view(x, y, tol)
{
  module duplicate()
  {
    translate([-x,-y]/2)
      children();

    translate([+x,-y]/2)
      mirror([1, 0])
        children();
  }

  difference()
  {
    chamfer(radius_fillet)
      fillet(radius_fillet)
        difference()
        {
          square([x, y], center = true);

          // remove the notch
          duplicate()
            square([notch_width + tol, notch_height]);
        }

    // make walls lighter
    duplicate()
    {
      wd = notch_width - wall;
      ya = y - notch_height;
      translate([0, notch_height + reinforcement])
        polygon([ [-1, -1],
                  [wd, wd],
                  [wd, ya - 2*reinforcement - wd],
                  [-1, ya - 2*reinforcement + 1]]);
    }

    // make the bottom edge round
    duplicate()
    {
      ri = radius_fillet*sqrt(2);
      translate([notch_width + tol,0])
        polygon([ [-1, -1],
                  [-1, ri],
                  [ri, -1]]);
    }
  }
}

// box whose inner area is full
module box_full(x, y, z, tol, x_braces, y_braces)
{
  intersection()
  {
    rotate([90, 0, 0])
      translate([0, 0, -y/2])
        linear_extrude(y)
          side_view(x, z, tol);

    rotate([90, 0, 90])
      translate([0, 0, -x/2])
        linear_extrude(x)
          side_view(y, z, tol);
  }

  module x_mirror()
  {
    children();
    mirror([0,1,0])
      children();
  }

  module y_mirror()
  {
    children();
    mirror([1,0,0])
        children();
  }

  // corner supports
  x_mirror() y_mirror()
    translate(notch_height*[1,1,2]/2
              + [0,0,radius_fillet]
              - [x,y,0]/2)
      rotate([0,0,45])
        cube( [3*notch_width,2*reinforcement,0] + [0,0,z],
              center=true);

  for (i = x_braces)
    x_mirror()
      translate([i,
                 y/2 - notch_width*sqrt(2) + reinforcement/sqrt(2)/2,
                 notch_height + radius_fillet])
        rotate([0, 0, 45])
          cube(2*notch_width*[1,1,0] + [0,0,z], center = true);

  for (i = y_braces)
    y_mirror()
      translate([x/2 - notch_width*sqrt(2) + reinforcement/sqrt(2)/2,
                 i,
                 notch_height + radius_fillet])
        rotate([0, 0, 45])
          cube(2*notch_width*[1,1,0] + [0,0,z], center = true);
}

module box(x = 120, y = 80, z = 60, hole = true, x_braces = [], y_braces = [])
{
    assert(wall < notch_width);

    difference()
    {
      intersection()
      {
        box_full(x, y, z, tolerance, x_braces, y_braces);
        round_cube( [x,y,z],
                    radius = sqrt(2)*radius_fillet + 0.1,
                    fn = 8);
      }

      // main inner area
      translate([0, 0, notch_height])
        chamfer_cube( [x,y,z] - [2,2,0]*notch_width,
                      radius = radius_inner);
      // additional inner area
      translate([0, 0, base])
        chamfer_cube( [x,y,z] - [2,2,0]*(notch_width+base),
                      radius = radius_inner);

      echo(str("External size: ", x, " x ", y, " x ", z, " mm"));
      echo(str("Internal size: ",
          x - 2*notch_width, " x ",
          y - 2*notch_width, " x ",
          z - base, " mm"));

      // chamfer the top-inner edge
      translate([0, 0, z - notch_height])
        box_full(x, y, z, 0, [], []);

      // hole for holding the box
      if (hole)
        translate([0, 0, z/2 - (hole_height/2 + reinforcement + protrusion() + radius_fillet)])
        {
          // main hole
          hole_dims = [x, hole_width, hole_height];
          cube(hole_dims, center = true);

          cham_dims = hole_dims + radius_fillet*[2,2,2];
          // chamfer internal edges
          chamfer_cube( cham_dims - notch_width*[2,0,0],
                        radius = radius_fillet,
                        vertical = true);

          // chamfer external edges
          translate(+[x - protrusion(), 0, 0])
            chamfer_cube( cham_dims,
                          radius = radius_fillet,
                          vertical = true);

          translate(-[x - protrusion(), 0, 0])
            chamfer_cube( cham_dims,
                          radius = radius_fillet,
                          vertical = true);
        }
    }
}
