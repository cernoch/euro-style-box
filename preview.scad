use <library.scad>;

difference()
{
    box(x = 180, y = 140, z = 100, x_braces = [-70,+70], y_braces = [-28,+28]);
    translate([0, 0, -50])
        cube([120, 80, 100]);
}
