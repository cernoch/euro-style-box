STL:\
preview.stl\
box-full.stl\
box-hlf1.stl\
box-hlf2.stl\
box-qart.stl\
box-eigt.stl\

OPENSCAD=openscad
# Uncomment for Windows
# OPENSCAD="C:\Program Files\OpenSCAD\openscad.exe"

all: $(STL)

%.stl: %.scad library.scad
	$(OPENSCAD) -o $@ $<

clean:
	rm -f $(STL)
