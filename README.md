Euro container inspired boxes
=============================

This project
- contains stackable boxes;
- suitable for 3D printing;
- fit snugly into a [Euro container](https://en.wikipedia.org/wiki/Euro_container) (aka Kleinladungsträger, KLT);
- uses parametric design thanks to [OpenSCAD](https://www.openscad.org/).

![Preview and cross-section of the container](preview.png)

Ready-to-download sizes
-----------------------

You can use these boxes using the default, pre-compiled `stl` files.

| Name    | Internal dimensions | External dimensions | Hole for carying | Download            | Preview                          |
|---------|--------------------:|--------------------:|-----------------:|:-------------------:|:--------------------------------:|
| eigth   |     74 x 74 x 52 mm |     88 x 88 x 55 mm |             none | [`box-eigt.stl`](box-eigt.stl) |![box-eigt.png](box-eigt-150.png) |
| quarter |    170 x 74 x 52 mm |    184 x 88 x 55 mm |             none | [`box-qart.stl`](box-qart.stl) |![box-qart.png](box-qart-150.png) |
| half 2  |   170 x 170 x 52 mm |   184 x 184 x 55 mm |             none | [`box-hlf2.stl`](box-hlf2.stl) |![box-half.png](box-hlf2-150.png) |
| half 1  |    170 x 74 x 97 mm |   184 x 88 x 100 mm |       40 x 20 mm | [`box-hlf1.stl`](box-hlf1.stl) |![box-half.png](box-hlf1-150.png) |
| full    |   170 x 170 x 97 mm |  184 x 184 x 100 mm |       40 x 20 mm | [`box-full.stl`](box-full.stl) |![box-full.png](box-full-150.png) |

These boxes are stackable:
- You can put two _eigth_ sized boxes on top of one _quarter_ (or _half 1_) sized boxes.
- You can put two _quarter_ (or _half 1_) sized boxes on top of one _full_ (or _half 2_) sized boxes.
- 2 _quarter_ sized boxes on top of each other are as high as 1 _half_ or _full_ sized box.


3D printing
-----------

Suggestions, in the order of priority:

1) The boxes are stackable. Therefore the bottom lip contains a 90° overhang that _needs supports_.
2) For a clean surface, _avoid supports near the box_. You will need to update the bridging angle:

![Use 45° bridging angle](bridge-angle.png)

3) The hole for handling the box is quite big. You may want to _add another support_.

Parametric design
-----------------

You can modify the external dimensions by the following call:

```openscad
use <library.scad>;
box(x = 180, /* width */ y = 180, /* depth */ z = 100, /* height */
    x_braces = [-70,+70], y_braces = [-28,+28] /* positions of additional bracing */
    hole = true, /* add the hole for holding the box by hand */ );
```

Filling Euro containers
-----------------------

The boxes snugly fill a [Euro container](https://en.wikipedia.org/wiki/Euro_container):

| Container | its floor area | # of _eigth_ boxes ... | ... _quarter_ or _half1_ ... | ... _half2_ or _full_ | Slack in the container | My rating |
|:---------:|:--------------:|:----------------------:|:----------------------------:|:---------------------:|:----------------------:|:---------:|
| smaller   |  270 x 370 mm  |           12           |                 -            |            -          |          6 x 18 mm     |   `**`    |
| smaller   |  270 x 370 mm  |            6           |                 3            |            -          |          6 x 10 mm     |   `***`   |
| smaller   |  270 x 370 mm  |            -           |                 6            |            -          |           6 x 2 mm     |   `*`     |
| bigger    |  370 x 570 mm  |            -           |                12            |            -          |         18 x 18 mm     |   `***`   |
| bigger    |  370 x 570 mm  |            -           |                 6            |            3          |         10 x 18 mm     |   `***`   |
| bigger    |  370 x 570 mm  |            -           |                 -            |            6          |          2 x 18 mm     |   `*`     |
| bigger    |  370 x 570 mm  |            8           |                 8            |            -          |         18 x 26 mm     |   `**`    |
| bigger    |  370 x 570 mm  |            4           |                 6            |            2          |         10 x 26 mm     |   `**`    |
| bigger    |  370 x 570 mm  |            -           |                 4            |            4          |          2 x 26 mm     |   `*`     |

**Note 1:** Configuration with 2mm slack have `*` rating.
The tolerances of Euro containers are bad. The boxes might not fit.

**Note 2:** Too few configurations for the _small_ container?
1. Decrease the size of the larger dimension from 185 mm to 180 mm.
2. Now you can fit 1 _eigth_ and 1 _quarter_ box along its shorter axis...
3. ...at the cost of much larger slack in other configurations.

FAQ
---

### 1. Why such strange dimensions?

There were conflicting goals. Ideally...
- The dimensions of the _quarter_ and _half 1_ boxes have 2:1 ratio.
- This ratio can be slightly bigger, but never smaller (stackability).
- The Euro container should be filled snugly.
- There should be many ways to fill the Euro container.

### 2. Why OpenSCAD?

It's free and it's fun.
